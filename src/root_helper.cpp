#include "root_helper.h"

#include <iostream>

#include "TMath.h"


namespace root_helper {
    
    
    TGraphErrors*
    derivative(const TGraphErrors* g)
    {
        unsigned int N = g->GetN();
        double*      X = g->GetX();
        double*      Y = g->GetY();
        double*     EX = g->GetEX();
        double*     EY = g->GetEY();
    
        TGraphErrors* gPrime = 0;
        std::vector<double> gPrime_Y, gPrime_EY, gPrime_EY_stat, gPrime_EY_trunc;
        
        double dy;
        double h = X[1] - X[0];   // Assume equally spaced data points
    
        // Lower Bound: Use 1st order accurate forward finite difference
        dy = Y[1] - Y[0];
        gPrime_Y.push_back(dy/h);
        gPrime_EY_stat.push_back(
            TMath::Sqrt( TMath::Sq(EY[1]) + TMath::Sq(EY[0]) ) / h );
        
        // Interior: Use 2nd order accurate central finite differences
        for (unsigned int i = 1; i < N-1; i++) {
            double dy = Y[i+1] - Y[i-1];
            gPrime_Y.push_back(dy/(2*h));
            gPrime_EY_stat.push_back(
                TMath::Sqrt( TMath::Sq(EY[i+1]/(2*h)) +
                             TMath::Sq(EY[i-1]/(2*h)) +
                             TMath::Sq(EX[i]*(Y[i-1]-2*Y[i]+Y[i+1])/(h*h))
                )
            );
        }
    
        // Upper Bound: Use 1st order accurate backward finite difference
        dy = Y[N-1] - Y[N-2];
        gPrime_Y.push_back(dy/h);
        gPrime_EY_stat.push_back(
            TMath::Sqrt( TMath::Sq(EY[N-1]) + TMath::Sq(EY[N-2]) ) / h );
    
        // Estimate truncation error: Use 3rd derivate 2st order accurate central
        // finite difference for interior and the according 2nd deriavtive 1st
        // order accurate fwd/bkw finite differences for the boundary points
        gPrime_EY_trunc.push_back( ( Y[0]-2*Y[1]+  Y[2]     )/(2*h) );
        gPrime_EY_trunc.push_back( (-Y[0]+3*Y[1]-3*Y[2]+Y[3])/(6*h) );
        for (unsigned int i = 2; i < N-2; i++)
            gPrime_EY_trunc.push_back((-0.5*Y[i-2]+Y[i-1]-Y[i+1]+0.5*Y[i+2])/(6*h));
        gPrime_EY_trunc.push_back( (-Y[N-4]+3*Y[N-3]-3*Y[N-2]+Y[N-1])/(6*h) );
        gPrime_EY_trunc.push_back( (          Y[N-3]-2*Y[N-2]+Y[N-1])/(2*h) );
    
        for (unsigned int i = 0; i < gPrime_EY_trunc.size(); i++)
            gPrime_EY.push_back( TMath::Sqrt( TMath::Sq(gPrime_EY_stat[i]) +
                                              TMath::Sq(gPrime_EY_trunc[i])
                                            )
                               );
        
        gPrime = new TGraphErrors(N, X, gPrime_Y.data(), EX, gPrime_EY.data());
        char name[128]; sprintf(name, "d1_%s", g->GetName());
        gPrime->SetName(name);
    
        return gPrime;
    }
    


    TGraphErrors*
    smooth(const TGraphErrors* g, unsigned int kernel_size)
    {
        unsigned int N = g->GetN();
        
        std::vector<double>  Y,  EY;
        std::vector<double> sY, sEY;
        
        // Extend arrays on both sides by kernel_size
        for (unsigned int i = 0; i < kernel_size; i++) {
            Y.push_back ( g->GetY()[0]  );
            EY.push_back( g->GetEY()[0] );
        }
        for (unsigned int i = 0; i < N; i++) {
            Y.push_back ( g->GetY()[i]  );
            EY.push_back( g->GetEY()[i] );
        }
        for (unsigned int i = 0; i < kernel_size; i++) {
            Y.push_back ( g->GetY()[N-1]  );
            EY.push_back( g->GetEY()[N-1] );
        }

        // Do the actual smoothing
        for (unsigned int i = 0; i < N; i++) {
            double mean = 0.0;
            double var  = 0.0;
            for (unsigned int k = 0; k <= 2*kernel_size; k++) {
                mean += Y[i+k] / (EY[i+k]*EY[i+k]);
                var  +=    1.0 / (EY[i+k]*EY[i+k]);
            }
            sY.push_back(mean/var);
            sEY.push_back(TMath::Sqrt(1.0/var));
        }

        return new TGraphErrors(N,
                                g->GetX(),
                                sY.data(),
                                g->GetEX(),
                                sEY.data());
    }
    
    
    
    TGraphErrors*
    treeDraw(TTree* tree, std::string varexp, std::string selection)
    {
        size_t dim = std::count(varexp.begin(), varexp.end(), ':') + 1;
        Long64_t N = tree->Draw(varexp.c_str(), selection.c_str(), "goff");
    
        TGraphErrors* graph = 0;
        switch(dim) {
            case 2:
                graph = new TGraphErrors(N, tree->GetV1(), tree->GetV2(), 0, 0);
                break;
            case 3:
                graph = new TGraphErrors(N, tree->GetV1(), tree->GetV2(),
                                         0, tree->GetV3());
                break;
            case 4:
                graph = new TGraphErrors(N, tree->GetV1(), tree->GetV2(),
                                         tree->GetV3(), tree->GetV4());
                break;
            default:
                std::cout << "knifeedge::treeDraw() invalid dimension of varexp: "
                          << varexp << std::endl;
                break;
        }
        return graph;
    }
    
    
    
    void
    format_line(TAttLine* line, int col, int sty, int width)
    {
        line->SetLineColor(col);
        line->SetLineStyle(sty);
        line->SetLineWidth(width);
    }
    
    
    
    void
    format_marker(TAttMarker* marker, int col, int sty, float size)
    {
        marker->SetMarkerColor(col);
        marker->SetMarkerStyle(sty);
        marker->SetMarkerSize(size);
    }
    

}
