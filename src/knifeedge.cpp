#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <algorithm>

#include "TGraphErrors.h"
#include "TMath.h"
#include "TBranch.h"

#include "knifeedge.h"
#include "root_helper.h"
#include "beam.h"


namespace knifeedge {
    
    
    TTree*
    readFile(std::string filename, float dzpos, float dpos, float dI)
    {
        std::vector<std::string> files({filename});
        return readFiles(files, dzpos, dpos, dI);
    }
    
    
    
    TTree*
    readFiles(std::vector<std::string> filenames, float dzpos, float dpos, float dI)
    {
        unsigned char      orientation;
        float              zpos, py_width, py_dwidth, py_width9010, py_dwidth9010;
        float              truth_width=-9999.9;
        std::vector<float> v_pos, v_dpos, v_I, v_dI;
    
        TTree* p_tree = new TTree("KE_datasets", "KnifeEdge datasets tree");
        p_tree->SetDirectory(0);
        p_tree->Branch("orientation"   , &orientation  );
        p_tree->Branch("zpos"          , &zpos         );
        p_tree->Branch("dzpos"         , &dzpos        );
        p_tree->Branch("py_width"      , &py_width     );
        p_tree->Branch("py_dwidth"     , &py_dwidth    );
        p_tree->Branch("py_width9010"  , &py_width9010 );
        p_tree->Branch("py_dwidth9010" , &py_dwidth9010);
        p_tree->Branch("truth_width"   , &truth_width  );
        p_tree->Branch("pos"           , &v_pos        );
        p_tree->Branch("dpos"          , &v_dpos       );
        p_tree->Branch("I"             , &v_I          );
        p_tree->Branch("dI"            , &v_dI         );
    
        for (auto &filename : filenames){
            std::ifstream infile(filename);
            std::string line, null;
            v_pos.clear(); v_dpos.clear(); v_I.clear(); v_dI.clear();
            while (std::getline(infile, line)) {
                if (line[0] == '#') {    // HEADER
                    std::stringstream ss(line);
                    if (line.find(" orientation:") != std::string::npos)
                        ss >> null >> null >> orientation;
                    else if (line.find(" zpos:") != std::string::npos)
                        ss >> null >> null >> zpos;
                    else if (line.find(" width:") != std::string::npos)
                        ss >> null >> null >> py_width >> null >> py_dwidth;
                    else if (line.find(" width9010:") != std::string::npos)
                        ss >> null >> null >> py_width9010 >> null >> py_dwidth9010;
                    else if (line.find(" truth_width:") != std::string::npos)
                        ss >> null >> null >> truth_width;
                } else {   // CSV BODY
                    float pos, I;
                    sscanf(line.c_str(), "%f,%f", &pos, &I);
                    v_pos.push_back(pos); v_dpos.push_back(dpos);
                    v_I.push_back(I);     v_dI.push_back(dI);
                }
            }
            std::cout << "Adding: " << filename << std::endl;
            if (v_pos.empty())
                std::cout << "\tWarning: No Datapoints found!" << std::endl;
            
            p_tree->Fill();
            
            infile.close();
        }

        p_tree->ResetBranchAddresses();
        
        return p_tree;
    }
    
    
    
    TF1
    fitWidth(TGraphErrors* g, Option_t* option, float rmin, float rmax)
    {
        unsigned int N     = g->GetN();
        float        x_min = TMath::MinElement(N, g->GetX());
        float        x_max = TMath::MaxElement(N, g->GetX());
    
        TF1 beam = Beam::fWidth(TMath::MaxElement(N, g->GetY()),
                                g->GetMean(),
                                0.5*(x_max - x_min),
                                TMath::MinElement(N, g->GetY()),
                                x_min,
                                x_max); 
        
        g->Fit(&beam, option, "", rmin, rmax);
        
        return beam;
    }



    void
    fitWidth9010(TGraphErrors* g, float &value, float& uncert)
    {
        const unsigned int kSteps = 1000;
        const float        kScale = 1.0 / 1.2815515655446; // norm.ppf(0.9)
        
        unsigned int N  = g->GetN();
        double*      X  = g->GetX();
        double*      Y  = g->GetY();
        double*     EX  = g->GetEX();
        double*     EY  = g->GetEY();

        double min = TMath::Mean(8, &Y[0]);  double max = TMath::Mean(8, &Y[N-8-1]);
        double clip10 = 0.1*(max-min) + min; double clip90 = 0.9*(max-min) + min;

        int     i10 = 0; int     i90 = 0;
        double  x10 = 0; double  x90 = 0;
        double dx10 = 0; double dx90 = 0;

        // closest datapoint _below_ the clipping levels
        for (unsigned int i = 0; i < N; i++) {
            if (Y[i] < clip10) i10 = i;
            if (Y[i] < clip90) i90 = i;
        }
        
        // stepsize, slope (+uncertainty) for linear interpolation
        double sx10 = (X[i10+1] - X[i10]) / (float) kSteps;
        double sx90 = (X[i90+1] - X[i90]) / (float) kSteps;
        double  m10 = (Y[i10+1] - Y[i10]) / (X[i10+1] - X[i10]);
        double  m90 = (Y[i90+1] - Y[i90]) / (X[i90+1] - X[i90]);
        double dm10 = TMath::Sqrt( TMath::Sq(EY[i10+1]/(X[i10+1]-X[i10])) +
                                   TMath::Sq(EY[i10  ]/(X[i10+1]-X[i10])) +
                                   TMath::Sq(m10*EX[i10+1]/(X[i10+1]-X[i10])) + 
                                   TMath::Sq(m10*EX[i10  ]/(X[i10+1]-X[i10])));
        double dm90 = TMath::Sqrt( TMath::Sq(EY[i90+1]/(X[i90+1]-X[i90])) +
                                   TMath::Sq(EY[i90  ]/(X[i90+1]-X[i90])) +
                                   TMath::Sq(m90*EX[i90+1]/(X[i90+1]-X[i90])) + 
                                   TMath::Sq(m90*EX[i90  ]/(X[i90+1]-X[i90])));
        
        // interpolated data point at clipping levels
        // (errors on x and slope are correlated)
        for (unsigned int i = 0; i < kSteps; i++) {
            if (Y[i10] + m10*sx10*i < clip10) {
                x10  = X[i10] + sx10*i;
                dx10 = EX[i10] + sx10*i*dm10/m10;
            }
            if (Y[i90] + m90*sx90*i < clip90) {
                x90 = X[i90] + sx90*i;
                dx90 = EX[i90] + sx90*i*dm90/m90;
            }
        }

        value  = (x90 - x10) * kScale;
        uncert = TMath::Sqrt( dx90*dx90 + dx10*dx10) * kScale;
    }



    TGraphErrors*
    fitWidthDeriv(TGraphErrors* g,
                  float &value,
                  float& uncert,
                  const int kSmooth)
    {
        TGraphErrors* gd = root_helper::derivative(
            root_helper::smooth(g, kSmooth));
        
        unsigned int N = gd->GetN();

        TF1 gauss("gauss", "gaus(0)", gd->GetX()[0], gd->GetX()[N-1]);
        gauss.SetParameter(0, TMath::MaxElement(N, gd->GetY()));
        gauss.SetParameter(1, gd->GetMean());
        gauss.SetParameter(2, gd->GetRMS());
        gd->Fit(&gauss);

        value  = 2.*gauss.GetParameter(2); // factor 2 for width definition
        uncert = 2.*gauss.GetParError(2);

        return gd;
    }
    
    
    
    TF1
    fitWaist(TGraphErrors* g, Option_t* option)
    {
        unsigned int N     = g->GetN();
        float        z_min = TMath::MinElement(N, g->GetX());
        float        z_max = TMath::MaxElement(N, g->GetX());
        
        TF1 beam = Beam::fWaist(TMath::MinElement(N, g->GetY()),
                                0.1*(z_max - z_min),
                                g->GetX()[TMath::LocMin(N, g->GetY())],
                                z_min,
                                z_max);
    
        g->Fit(&beam, option);
        
        return beam;
    }
    
    
    
    std::vector<TGraphErrors*>
    treeGetWidthGraphs(TTree* tree)
    {
        std::vector<TGraphErrors*> graphs;
        unsigned int        N = tree->GetEntries();
        unsigned char       orientation = 0;
        float               zpos;
        std::vector<float>* pos  = 0;
        std::vector<float>* I    = 0;
        std::vector<float>* dpos = 0;
        std::vector<float>* dI   = 0;
    
        tree->SetBranchAddress("orientation",&orientation);
        tree->SetBranchAddress("zpos"       ,&zpos);
        tree->SetBranchAddress("pos"        ,&pos );
        tree->SetBranchAddress("I"          ,&I   );
        tree->SetBranchAddress("dpos"       ,&dpos);
        tree->SetBranchAddress("dI"         ,&dI  );
    
        for (unsigned int i = 0; i < N; i++) {
            tree->GetEntry(i);
    
            TGraphErrors* g = new TGraphErrors(pos ->size(),
                                               pos ->data(),
                                               I   ->data(),
                                               dpos->data(),
                                               dI  ->data());
            char name[80]; sprintf(name, "gWidth_%c%.4f", orientation, zpos);
            g->SetName(name); graphs.push_back(g);
        }
    
        tree->ResetBranchAddresses();
    
        return graphs;
    }
    

    
}
