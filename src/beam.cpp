#include "beam.h"


namespace knifeedge {

    
    TF1
    Beam::fWidth(double P, double mu, double w, double c, double xmin, double xmax)
    {
        TF1 f("fWidth",
              "[3] + 0.5*[0] * (1 + TMath::Erf( TMath::Sqrt2()*(x-[1])/[2] ))",
              xmin, xmax);
        
        f.SetParNames("P", "mu", "w", "c");
        f.SetParameters(P, mu, w, c);
        f.SetNpx(250);
    
        return f;
    }
    
    
    
    TF1
    Beam::fWaist(double w, double zR, double z0, double zmin, double zmax)
    {
        TF1 f("fWaist",
              "[0]*TMath::Sqrt(1.0+TMath::Sq((x-[2])/[1]))",
              zmin, zmax);
        
        f.SetParNames("w","zR","z0");
        f.SetParameters(w, zR, z0);
        f.SetNpx(250);
    
        return f;
    }
    

}
