/**
 *   \file knifeedge.h
 *   \brief Utility functions to handle and analyze KnifeEdge data
 *
 *  This library provides utility functions to perform basic I/O operations of
 *  KnifeEdge Data and ROOT and methods to analyse the data in terms of gaussian
 *  optics.
 */

#ifndef KNIFEEDGE_H
#define KNIFEEDGE_H

#include "TTree.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"



/**
 *  \brief Global libknifeedge namespace
 */
namespace knifeedge {

    
    /**
     *  \brief Read KnifeEdge data from a single csv file
     *
     *  See knifeedge::readFiles() for details
     */
    TTree* readFile (std::string filename,
                     float dzpos=0.0, float dpos=0.0, float dI=0.0);
    
    
    /**
     *  \brief Read KnifeEdge data from csv files
     *
     *  Read Knife Edge data from csv files. The commented header (orientation,
     *  zpos, ...) of each file is extracted as well and is stored as seperate
     *  branches in the tree. The actual datapoints are stored as std::vector of
     *  floats. You may provide additional values to be used as uncertainties
     *  that are not contained in the data files.
     *
     *  \param files vector of abs/rel filepaths
     *  \param dzpos optional uncertainty on z-position
     *  \param dpos  optional uncertainty on vertical/horizontal position
     *  \param dI    optional uncertainty on intensity
     *
     *  \return TTree containing the data
     */
    TTree* readFiles(std::vector<std::string> files,
                     float dzpos=0.0, float dpos=0.0, float dI=0.0);
    
    
    /**
     *  \brief Perform a width profile fit
     *
     *  Fit a width profile function from knifeedge::Beam::fWidth() to a given
     *  dataset. The fit options are passed to the TGraphErrors::Fit() method
     *  and you may additionally specify a fit range.
     *
     *  \param g      width profile data to fit to
     *  \param option string of options passed to TGraphErrors::Fit()
     *  \param xmin   lower bound of optional fit range
     *  \param xmax   upper bound of optional fit range
     *
     *  \return fitted width profile TF1 object
     */
    TF1 fitWidth(TGraphErrors* g, Option_t* option = "",
                 float xmin=0.0, float xmax=0.0);


    /**
     *  \brief Estimate width based on 90%-10% method
     *
     *  Estimate the width from the width profile using the 90% - 10% clipping
     *  method.
     *
     *  \param g      width profile data
     *  \param value  variable to store the width in
     *  \param uncert variable to store the width uncertainty in
     */
    void fitWidth9010(TGraphErrors* g, float &value, float& uncert);


    /**
     *  \brief Estimate width based on the gaussian fitted derivative
     *
     *  Builds the derivative of the width profile data and fits the resulting
     *  line shape with a gaussian distribution. The derivative ist calculated
     *  numerically via root_helper::derivative() based on finite differences.
     *
     *  CAVEAT: Using large smoothing kernels leads to overestimation of the
     *          beam width.
     *
     *  \param g       width profile data
     *  \param value   variable to store the width in
     *  \param uncert  variable to store the width uncertainty in
     *  \param kSmooth Kernel size for smoothing before building the derivative
     *
     *  \return derivative of the width profile
     */
    TGraphErrors* fitWidthDeriv(TGraphErrors* g,
                                float &value,
                                float& uncert,
                                const int kSmooth=1);
    
    
    /**
     *  \brief Perform a waist profile fit
     *
     *  Fit a waist profile function from knifeedge::Beam::fWaist() to a given
     *  daataset. The fit options are passed to the TGraphErrors::Fit() method.
     *
     *  \param g      waist profile data to fit to
     *  \param option string of options passed to TGraphErrors::Fit()
     *
     *  \return fitted waist profile TF1 object
     */
    TF1 fitWaist(TGraphErrors* g, Option_t* option = "");


    /**
     *  \brief Draw all width profiles in a tree as TGraphErrors
     *
     *  Create a TGraphErrors representation of the width profile for each entry
     *  in the tree and return all of them as std vector. This assumes the tree
     *  containing branches as defined in knifeedge::readFiles()
     *
     *  \param tree tree containing the datasets
     *  \return vector of width profiles in TGraphErrors
     */
    std::vector<TGraphErrors*> treeGetWidthGraphs(TTree* tree);

    
}


#endif /* KNIFEEDGE_H */
