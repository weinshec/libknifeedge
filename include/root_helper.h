/**
 *   \file root_helper.h
 *   \brief This file defines the root_helper namespace
 *
 *  The root_helper namespace provides handy functions to deal with various
 *  kinds of tasks in ROOT.
 */

#ifndef ROOT_HELPER_H
#define ROOT_HELPER_H


#include "TGraphErrors.h"
#include "TTree.h"



/**
 *  \brief Namespace for convenient root helper macros
 */
namespace root_helper {
    
    
    /**
     *  \brief Build the numerical derivative of a TGraphErrors based on finite
     *  differences
     *
     *  Numerical derivation is done via 2nd order accurate central finite
     *  differences in the interior and 1st order accurate forward/backward
     *  finite differences at the boundaries. In general the datapoints are
     *  assumed to be equally spaced.
     * 
     *  Uncertainties are estimated taking errors in x and y direction into
     *  account treating them as uncorrelated in the error propagation for the
     *  statistical error. In addition the truncation error arising from the
     *  neglection of higher order terms in the taylor expansion is estimated by
     *  calculating the next to leading order contribution.
     *
     *  \param g data to build the derivative
     * 
     *  \return first derivative
     */
    TGraphErrors* derivative(const TGraphErrors* g);


    /**
     *  \brief Perform linear smoothing with user defined kernel size
     *
     *  The kernel size must be >= 1 and defines how many left and right
     *  neighbours are taken into account for averaging. At the borders the last
     *  value is repeated. For smoothing the weighted moving average is taken.
     *
     *  \param g           input data
     *  \param kernel_size number points left and right to use for averaging
     *
     *  \return smoothed data
     */
    TGraphErrors* smooth(const TGraphErrors* g, unsigned int kernel_size = 1);
    

    /**
     *  \brief Create a TGraphErrors from tree data utilizing the TTree::Draw()
     *  method
     *
     *  Convenience wrapper around TTree::Draw() to allow quick TGraphErrors
     *  creation from data stored in trees. Use `varexp` to define the data to
     *  be drawn. E.g. "pos:I:dI" creates the graph plotting I vs. pos using dI
     *  as y-uncertainties. You can perform operations in `varexp` as allowed by
     *  the TTree::Draw() method (e.g. "pos:sqrt(I)"). You are allowed to
     *  specify at most 4 branched in `varexp` meaning "x:y:dx:dy".
     *
     *  \param tree      input tree containing the data to draw
     *  \param varexp    expression specifying what to draw
     *  \param selection expression for additional cuts on Tree data
     *
     *  \return plot of the specifyied data
     */
    TGraphErrors* treeDraw(TTree* tree,
                           std::string varexp,
                           std::string selection = "");
    

    /**
     *  \brief Set line attributes like color, style and width
     *
     *  Convenience function setting the most important line style attributes in
     *  a single function call.
     *
     *  \param line  Line to set the attributes for
     *  \param col   line color
     *  \param sty   line style
     *  \param width line width
     */
    void format_line(TAttLine* line, int col, int sty, int width=2.0);

    
    /**
     *  \brief Set marker attributes like color, style and size
     *
     *  Convenience function setting the most important marker style attributes
     *  in a single function call.
     *
     *  \param marker marker to set the attributes for
     *  \param col    marker color
     *  \param sty    marker style
     *  \param size   marker size
     */
    void format_marker(TAttMarker* marker, int col, int sty, float size=1.0);

    
    /**
     *  \brief Add a new Branch to an existing tree
     *
     *  Allows to add a new branch to an existing tree given its name and a std 
     *  vector of arbitrary type and of length equal to the number of tree
     *  entries.
     *
     *  \param orig the original tree to add the branch to
     *  \param name name of the new branch
     *  \param var  std vector containing the data to fill the branch
     */
    template <typename T> void treeAddBranch(TTree* orig,
                                             std::string name,
                                             std::vector<T> const& var)
    {
        T new_var;
        TBranch *newBranch = orig->Branch(name.c_str(), &new_var);

        for (Long64_t i = 0; i < orig->GetEntries(); i++){
            new_var = var.at(i); newBranch->Fill();
        }
        orig->ResetBranchAddresses();
    }

    
    
}  // root_helper



#endif /* ROOT_HELPER_H */
