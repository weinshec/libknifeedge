/**
 *   \file beam.h
 *   \brief Definition of the Beam class representing a gaussian beam
 *
 *  The class represents a gaussian beam in terms of its beam parameter.
 *
 */

#ifndef BEAM_H
#define BEAM_H


#include "TF1.h"



namespace knifeedge {
    
    
    /**
     *  \brief Representing a gaussian beam
     *
     *  The class represents a gaussian beam on terms of its beam parameter
     */
    class Beam
    {
        
    private:
        float m_waist;
        float m_wavelength;
        TF1   m_TF1;
        
        
    public:
        Beam();
        virtual ~Beam();

        /**
         *  \brief Create a width profile function object
         *
         *  Create a TF1 object describing the width profile given by
         *
         *  \f[
         *      I(x) = \frac{P}{2} \left(
         *          1 + \text{erf}\left[ \frac{x-\mu}{w} \right]
         *      \right)
         *  \f]
         *
         *  \param P    total power of the beam
         *  \param mu   position of the mean of the underlying gaussian
         *  \param w    width at the \f$ 1/e^2 \f$ level
         *  \param c    general constant offset
         *  \param xmin lower bound for the range
         *  \param xmax upper bound for the range
         *
         *  \return TF1 function object describing the width profile
         */
        static TF1 fWidth(double P,
                          double mu,
                          double w,
                          double c,
                          double xmin,
                          double xmax);

        /**
         *  \brief Create a waist profile function object
         *
         *  Create a TF1 object describing the waist profile given by
         *
         *  \f[
         *      w(z) = w \sqrt{1 + \left( \frac{z-z_0}{z_R} \right)^2 }
         *  \f]
         *
         *  \param w    beam waist (minimum beam width)
         *  \param zR   rayleigh range of the beam
         *  \param z0   z-position of the beam waist
         *  \param zmin lower bound for the range
         *  \param zmax upper bound for the range
         *
         *  \return TF1 function object describing the waist profile
         */
        static TF1 fWaist(double w,
                          double zR,
                          double z0,
                          double zmin,
                          double zmax);

        
    };
    
    
}  // knifeedge





#endif /* BEAM_H */
